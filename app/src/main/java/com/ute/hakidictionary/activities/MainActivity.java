package com.ute.hakidictionary.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.ute.hakidictionary.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}